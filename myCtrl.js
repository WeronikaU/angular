app.controller("myCtrl", function($scope) {
    //zakładam wartość eur jako null
    $scope.euro = null;
    //deklaruję resultArray jako tablicę
    $scope.resultArray = [];

    $scope.addAction = function(){
        //ustanowienie zmiennego kursu waluty, po każdnym kolejnym kliknięciu zmienia się kurs
    	var pln = 4.2*(1 + ((Math.floor(Math.random()*100)) - 50)/1000);
        //przeliczenie waluty euro na złotówki
    	var result = $scope.euro * pln;
    	//założenie, że zaokrąglenie jest do 4 miejsc po przecinku
	    result = result.toFixed(4);
    	//ustalamy zmienną dla aktualnej daty i czasu
    	var currentDate = new Date();
        //dodanie do tablicy wyników kolejnych elementów grid
        $scope.resultArray.push({date: currentDate, pln: result});
        //zerowanie zmiennej euro
        $scope.euro = null;
    }
}); 



/*miłego dnia :)*/